/**
 * Created by Filip Drgoň on 18/03/18.
 */

import * as admin from 'firebase-admin';
import serviceAccount from '../serviceAccountKey';

const connectToDb = () => {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://purple-tech-converter.firebaseio.com",
    });
};

export default connectToDb;