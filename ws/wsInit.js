import ws from "ws";
import * as admin from 'firebase-admin';

/**
 * Created by Filip Drgoň on 18/03/18.
 */

const websocket = (server) => {
    const wss = new ws.Server({server});
    let observer;
    wss.on('connection', (ws) => {
        //Statistics logic

        let db = admin.firestore();
        let collection = db.collection('currencies');
        observer = collection.onSnapshot(async (snapshot) => {
            let max = {amount: 0, name: "--"};
            let total = {};
            snapshot.forEach(shot => {
                if (shot.id !== "total") {
                    const data = shot.data();
                    if (data.amount > max.amount) max = {name: shot.id, amount: data.amount};
                } else {
                    total = shot.data();
                }
            });
            wss.clients.forEach(client => {
                client.send(JSON.stringify({max, total}));
            });
        });

        ws.on('close', () => {
            observer();
        })
    });
};

export default websocket;