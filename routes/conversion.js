/**
 * Created by Filip Drgoň on 16/03/18.
 */
import express from "express";
import fetch from "node-fetch";
import * as admin from 'firebase-admin';
import currencyList from "../utils/CurrencyList";
import isNumeric from 'isnumeric';

const router = express.Router();

const apiKey = "0520d9f4e7354ed1b6a9519b82dcd3d2";

router.post('/', async (req, res) => {
    const {from, to, amount} = req.body;
    try {
        //Validation
        if (!(currencyList.find((item) => item === from) && currencyList.find((item) => item === to))) {
            res.status(400);
            return res.json({error: "INVALID_CURRENCY", message: "One or more currency codes provided are invalid."});
        }
        if (!isNumeric(amount)) {
            res.status(400);
            return res.json({error: "INVALID_AMOUNT", message: "The amount provided is invalid."})
        }
        if (Math.sign(amount) !== 1) {
            res.status(400);
            return res.json({error: "NEGATIVE_AMOUNT", message: "The amount provided cannot be a negative number."})
        }

        //Data fetch
        const response = await fetch(`https://openexchangerates.org/api/latest.json/?app_id=${apiKey}&symbols=${from}%2C${to}`);
        const json = await response.json();
        const {rates} = json;
        const result = amount / rates[from] * rates[to];

        //Add or update destination currency counter in DB
        let db = admin.firestore();
        let destinationRef = db.collection('currencies').doc(to);
        let destination = await destinationRef.get();
        if (destination.exists) {
            const {amount: destinationAmount} = destination.data();
            destinationRef.update({amount: destinationAmount + 1});
        } else destinationRef.set({amount: 1});

        //Increment totalAmount and totalRequests counters in DB
        let totalRef = db.collection('currencies').doc('total');
        let total = await totalRef.get();
        const {totalAmount, totalRequests} = total.data();
        totalRef.update({
            totalAmount: totalAmount + (amount / rates[from]),
            totalRequests: totalRequests + 1,
        });

        //Send back result
        res.json({result});
    } catch (ex) {
        console.log(ex);
    }
});

export default router;